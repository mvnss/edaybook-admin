﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using eDaybookWebAdmin.Models;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.WindowsAzure;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;



namespace eDaybookWebAdmin.Controllers
{



    public class ClientController : Controller
    {
      //  static  MobileServiceClient mobileService = new MobileServiceClient("http://localhost:59475/");
        static  MobileServiceClient mobileService = new MobileServiceClient("https://edaybook-prototype.azure-mobile.net/","ePmepNWAmpAiUpCrOdyRHgSoAUZqxO52");
        IMobileServiceTable<Client> iClientMobileServiceTable = mobileService.GetTable<Client>();

        public async Task<ActionResult> Index()
        {
            List<Client> objClientList = null;

            try
            {
                objClientList = await iClientMobileServiceTable.ToListAsync();
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine("ERROR! : {0}", err.Message);
            }
            return View(objClientList.ToList());
        }

        public ActionResult AddClient()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddClient(Client clientDetails)
        {

           
            Client objClient = new Client()
            {

            //  Id is inserted by default at table level     
                Title             =       clientDetails.Title,
                Firstname         =       clientDetails.Firstname,
                Lastname          =       clientDetails.Lastname,
                Address1          =       clientDetails.Address1,
                Address2          =       clientDetails.Address2,


                City              =       clientDetails.City,
                County            =       clientDetails.County,
                Postcode          =       clientDetails.Postcode,
                Telephone         =       clientDetails.Telephone,
                Email             =       clientDetails.Email,


                Keyinfo           =       clientDetails.Keyinfo,
                Recenthistory     =       clientDetails.Recenthistory,
                Socialhistory     =       clientDetails.Socialhistory,
                Healthhistory     =       clientDetails.Healthhistory,
                Riskassess        =       clientDetails.Riskassess,


                Circlesupp        =       clientDetails.Circlesupp,

                Imageuri          =        string.Empty, 
                Sasquerystring    =        string.Empty,
                Containername     =       "clientimages",
                Resourcename      =       Guid.NewGuid().ToString() + ".jpg"
               
                
            };

          
            await iClientMobileServiceTable.InsertAsync(objClient);
           

            // UPLOAD IMAGE OF THE CLIENT

            foreach (string filesData in Request.Files)
            {
                HttpPostedFileBase fileClientImage = Request.Files[filesData];

                using (var fileStream = fileClientImage.InputStream)
                {

                    StorageCredentials cred = new StorageCredentials(objClient.Sasquerystring);
                    var imageUri = new Uri(objClient.Imageuri);

                    // Instantiate a Blob store container based on the info in the returned item.
                    CloudBlobContainer container =new CloudBlobContainer(new Uri(string.Format("https://{0}/{1}", imageUri.Host, objClient.Containername)), cred);

                    // Upload the new image as a BLOB from the stream.
                    CloudBlockBlob blobFromSASCredential = container.GetBlockBlobReference(objClient.Resourcename);

                    await blobFromSASCredential.UploadFromStreamAsync(fileStream);

                }
            }

            return RedirectToAction("Index", "Client");
        }

        [AllowAnonymous]
        public ActionResult EditClient()
        {
            var list = GetSingleClients();

            return View(list);
        }


        [HttpPost]
        public ActionResult EditClient(ClientModel clientDetails)
        {

            return RedirectToAction("AddClientVisit", "Client");
        }

        [AllowAnonymous]
        public ActionResult AddClientVisit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddClientVisit(ClientModel clientDetails)
        {

            return RedirectToAction("EditClientVisit", "Client");
        }



        public ActionResult EditClientVisit()
        {
            return View();
        }

        public List<ClientModel> GetClients()
        {
            var clientsList = new List<ClientModel>();
            clientsList.Add(new ClientModel { Title = "", Firstname = "Mrs Marigold Arnold", Lastname = "", Address1 = "", Address2 = "", City = "", County = "", Postcode = "", Telephone = "", Email = "", Keyinfo = "", Recenthistory = "", Socialhistory = "", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" });
            clientsList.Add(new ClientModel { Title = "", Firstname = "Mr Matt Campbell", Lastname = "", Address1 = "", Address2 = "", City = "", County = "", Postcode = "", Telephone = "", Email = "", Keyinfo = "", Recenthistory = "", Socialhistory = "", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" });
            clientsList.Add(new ClientModel { Title = "", Firstname = "Ms Deirdre Droke", Lastname = "", Address1 = "", Address2 = "", City = "", County = "", Postcode = "", Telephone = "", Email = "", Keyinfo = "", Recenthistory = "", Socialhistory = "", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" });
            clientsList.Add(new ClientModel { Title = "", Firstname = "Mr Erick Giles", Lastname = "", Address1 = "", Address2 = "", City = "", County = "", Postcode = "", Telephone = "", Email = "", Keyinfo = "", Recenthistory = "", Socialhistory = "", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" });
            clientsList.Add(new ClientModel { Title = "", Firstname = "Mr Leighton Hill", Lastname = "", Address1 = "", Address2 = "", City = "", County = "", Postcode = "", Telephone = "", Email = "", Keyinfo = "", Recenthistory = "", Socialhistory = "", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" });
            return clientsList;
        }
        public ClientModel GetSingleClients()
        {
            var clientsList = new ClientModel { Title = "", Firstname = "Mrs Marigold Arnold", Lastname = "", Address1 = "24 Giles Street", Address2 = "Greenton NewShire", City = "", County = "", Postcode = "NS13 5FT", Telephone = "01347 773459", Email = "", Keyinfo = "", Recenthistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Socialhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Healthhistory = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun", Riskassess = "", Circlesupp = "\r\nTerry Knightly - Family - 0135 248 255a\r\nPerin Huges - Family - 0210 445886", Containername = "", Resourcename = "", Sasquerystring = "", Imageuri = "" };
            return clientsList;
        }

        
    }

    
}