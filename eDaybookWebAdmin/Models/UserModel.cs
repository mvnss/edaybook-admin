﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eDaybookWebAdmin.Models
{
    public class UserModel
    {

        public string DisplayName { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
    }
}