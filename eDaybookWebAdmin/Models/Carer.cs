﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace eDaybookWebAdmin.Models
{
    public class Carer
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lasttname { get; set; }
        public string Username { get; set; }
    }
}